const fetch = require('node-fetch')
const request = require('request')
const ytdl = require('youtube-dl-exec')
const fs = require('fs')
const FormData = require('form-data');

const groupKey=""
const userKey="" // юзер должен иметь право на загрузку в сообщество
const apiVersion="5.131"
const groupID="" // сюда файл загрузит

async function fetchJSON(uri) {
  return (await fetch(uri)).json();
}

async function requireVKAPI(method, args) {
  const res = await fetch(
    `https://api.vk.com/method/${method}?access_token=${groupKey}&v=${apiVersion}&${args}`,
    {method: "post"}
  );
  return await res.json();
}

async function sendMessage(peer, text, from_id, attachment_id, message) {
     return requireVKAPI(
       "messages.send",
       `peer_id=${peer}&random_id=${Math.random() * 0xFFFFFFFFFF}&message=${encodeURI(text)}&attachment=${attachment_id}&disable_mentions=1`
     )
}


async function requireVKAPI_asUser(method, args) {
  const res = await fetch(
    `https://api.vk.com/method/${method}?access_token=${userKey}&v=${apiVersion}&${args}`,
    {method: "post"}
  );
  return await res.json();
}

async function sendVideo(filename, message) {
	const res = await requireVKAPI_asUser("video.save", `&name=${filename}&description=a&is_private=0&group_id=198657266&no_comments=1`);

	var jsonUpload = {  };
	var formData = {
    	'video_file': fs.createReadStream("./src/vids/" + filename),
	};

	var uploadOptions = {
    	"url": res.response.upload_url,
    	"method": "POST",
    	"headers": {
    	},
    	"formData": formData
	}

	var req = request(uploadOptions, async function(err, resp, body) {
    	if (err) {
        	console.log('Error ', err);
    	} else {
		body = JSON.parse(body)
		console.log(await sendMessage(message.peer_id, `@id${message.from_id} (lol)`, message.from_id, `video${body.owner_id}_${body.video_id}`));
    	}
	});
}

async function dl(link, message) {
	const filename = Math.floor(Math.random() * 100000000) + ".mp4"
    const vid = await ytdl(link, {output: true, output: "./src/vids/" + filename, format: true, format: 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best' });
	sendVideo(filename, message)
}

module.exports = {
	groupID:groupID,
	requireVKAPI: requireVKAPI,
	sendMessage: sendMessage,
	dl: dl,
	fetchJSON: fetchJSON
}
